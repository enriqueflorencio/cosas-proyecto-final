﻿-- Creación de la base de datos
  DROP DATABASE IF EXISTS proyectofinaldnd_version02;

  CREATE DATABASE proyectofinaldnd_version02;

  USE proyectofinaldnd_version02;

-- Creacion tablas
 -- Jugador

  CREATE OR REPLACE TABLE usuarios (
    id_usuario int AUTO_INCREMENT NOT NULL,
    nombre varchar(100) NOT NULL,
    icono varchar(200),
    email varchar(1000) NOT NULL,
    contraseña varchar(10) NOT NULL,
    PRIMARY KEY(id_usuario));

 -- Personaje

   CREATE OR REPLACE TABLE personajes (
    id_personaje int AUTO_INCREMENT NOT NULL,
    id_usuario int,
    id_armadura int,
    id_raza int, 
    id_trasfondo int,
    nombre varchar(100) NOT NULL,
    descripción text,
    imagen varchar(200),
    vivo bool DEFAULT TRUE NOT NULL,
    biografia text,
    edad int NOT NULL,
    nivel int NOT NULL DEFAULT 1,
    alineamineto enum('Legal bueno', 'Neutral bueno', 'Caótico bueno', 'Legal neutral', 'Neutral', 'Caótico neutral', 'Legal malvado', 'Neutral malvado', 'Caótico malvado'),
    fuerza int NOT NULL,
    destreza int NOT NULL,
    constitucion int NOT NULL,
    inteligencia int NOT NULL,
    sabiduria int NOT NULL,
    carisma int NOT NULL,
    modfuerza int,
    moddestreza int ,
    modconstitucion int,
    modinteligencia int,
    modsabiduria int,
    modcarisma int,
    vida int,
    iniciativa int,
    inspiracion bool DEFAULT FALSE,
    ca int,
    escudo bool DEFAULT FALSE,
    oro int DEFAULT 0,
    plata int DEFAULT 0,
    cobre int DEFAULT 0,
    velocidad int DEFAULT 30,
    salvfuerza int,
    salvdestreza int,
    salvconstitucion int,
    salvinteligencia int,
    salvsabiduria int,
    salvcarisma int,
    bono_competencia int DEFAULT 2,
    acrobacias int,
    atletismo int,
    conoarcano int,
    engaño int,
    historia int,
    interpretacion int,
    intimidacion int, 
    investigacion int,
    juego_de_manos int,
    medicina int,
    naturaleza int,
    percepcion int,
    perspicacia int,
    persuasión int,
    religion int,
    sigilo int,
    supervivencia int,
    trato_animales int,
    percepción_pasiva int,
    salv_conjuto int,
    mod_ataque_conjuro int,
    notas text,
    recomendado bool DEFAULT FALSE,
    PRIMARY KEY (id_personaje));

  -- idiomas del personaje

   CREATE OR REPLACE TABLE idiomaspersonaje (
    id int AUTO_INCREMENT NOT NULL,
    id_personaje int NOT NULL,
    idioma varchar(50),
    PRIMARY KEY (id));

  -- Armas

   CREATE OR REPLACE TABLE armas (
    id_arma int AUTO_INCREMENT NOT NULL,
    nombre varchar(100) NOT NULL,
    descripcion text,
    tipo varchar(50) NOT NULL,
    oro int,
    plata int,
    cobre int,
    daño varchar(100),
    magico bool DEFAULT FALSE,
    PRIMARY KEY (id_arma));

  -- Armas que tiene el personaje

   CREATE OR REPLACE TABLE armaspersonaje (
    id int AUTO_INCREMENT NOT NULL,
    id_personaje int NOT NULL,
    id_arma int NOT NULL,
    bonificador int NOT NULL,
    PRIMARY KEY (id));

  -- Armaduras

   CREATE OR REPLACE TABLE armaduras (
    id_armaduras int AUTO_INCREMENT NOT NULL,
    nombre varchar(100) NOT NULL,
    descripcion text,
    tipo varchar(20) NOT NULL,
    ca int,
    oro int,
    mágico bool NOT NULL DEFAULT FALSE,
    desventaja_sigilo bool NOT NULL,
    PRIMARY KEY (id_armaduras));

  -- Objetos

   CREATE OR REPLACE TABLE objetos (
    id_objeto int AUTO_INCREMENT NOT NULL,
    nombre varchar(100) NOT NULL,
    descripcion text,
    tipo varchar(50) NOT NULL,
    mágico bool NOT NULL DEFAULT FALSE,
    oro int,
    plata int, 
    cobre int,
    PRIMARY KEY (id_objeto));

  -- Objetos que tiene el personaje

   CREATE OR REPLACE TABLE objetosdelpersonaje (
    id int AUTO_INCREMENT NOT NULL,
    id_personaje int NOT NULL,
    id_objeto int NOT NULL,
    PRIMARY KEY (id));
    
  -- Razas

   CREATE OR REPLACE TABLE razas (
    id_raza int AUTO_INCREMENT NOT NULL,
    nombre varchar(100) NOT NULL,
    descripcion text,
    imagen varchar(200),
    velocidad int DEFAULT 30 NOT NULL,
    bonofuerza int DEFAULT 0,
    bonodestreza int DEFAULT 0,
    bonoconstitucion int DEFAULT 0,
    bonointeligencia int DEFAULT 0,
    bonosabiduria int DEFAULT 0,
    bonocarisma int DEFAULT 0,
    PRIMARY KEY (id_raza));

  -- Idiomas que hablan las razas

   CREATE OR REPLACE TABLE idiomasdelaraza (
    id int AUTO_INCREMENT NOT NULL,
    id_raza int NOT NULL,
    idioma varchar(50),
    PRIMARY KEY (id));

  -- Habilidades que tienen las Razas

   CREATE OR REPLACE TABLE tienenrazas (
    id int AUTO_INCREMENT NOT NULL,
    id_raza int NOT NULL,
    id_habilidadesraza int NOT NULL,
    PRIMARY KEY (id));

  -- Habilidades de la raza

   CREATE OR REPLACE TABLE habilidadesraza (
    id_habilidadraza int AUTO_INCREMENT NOT NULL,
    nombre varchar(100) NOT NULL,
    descripcion text,
    PRIMARY KEY (id_habilidadraza));

  -- Competencia en habilidades de personajes que te dan las clases

  CREATE OR REPLACE TABLE clasecomphabilidades (
    id int AUTO_INCREMENT NOT NULL,
    id_clase int NOT NULL,
    habilidad varchar(20) NOT NULL,
    PRIMARY KEY(id));
  
  -- Clases
    
   CREATE OR REPLACE TABLE clases (
    id_clase int NOT NULL AUTO_INCREMENT,
    nombre varchar(100) NOT NULL,
    descripcion text,
    imagen varchar(200),
    carateristica_principal varchar(50) NOT NULL,
    competencias text,
    dado_golpe varchar(10) NOT NULL,
    tipo varchar(200),
    aptitud_mágica enum('Carisma', 'Inteligencia', 'Sabiduria'),
    nivel_minimo int DEFAULT 1 NOT NULL,
    fuerza int DEFAULT 0 NOT NULL,
    destreza int DEFAULT 0 NOT NULL,
    constitucion int  DEFAULT 0 NOT NULL,
    inteligencia int DEFAULT 0 NOT NULL,
    sabiduria int DEFAULT 0 NOT NULL,
    carisma int DEFAULT 0 NOT NULL,
    PRIMARY KEY(id_clase));

  -- Salvaciones de la clase

  CREATE OR REPLACE TABLE salvaciones (
    id int AUTO_INCREMENT NOT NULL,
    id_clase int NOT NULL,
    salvaciones varchar(50) NOT NULL,
    PRIMARY KEY (id));

  -- Las clases que tienen los personajes

   CREATE OR REPLACE TABLE clasesdelospersonajes (
    id int AUTO_INCREMENT NOT NULL,
    id_clase int NOT NULL,
    id_personaje int NOT NULL,
    nivel_clase int NOT NULL,
    PRIMARY KEY (id));

  -- Habilidades que tienen las clases

   CREATE OR REPLACE TABLE tienenclase (
    id int AUTO_INCREMENT NOT NULL,
    id_clase int NOT NULL,
    id_habilidadesclase int NOT NULL,
    nivel_desbloque int NOT NULL,
    PRIMARY KEY (id));

  -- Habilidades de las clases

   CREATE OR REPLACE TABLE habilidadesclase (
    id_habilidadesclase int AUTO_INCREMENT NOT NULL,
    nombre varchar(100) NOT NULL,
    descripción text,
    PRIMARY KEY(id_habilidadesclase));

  -- Magias/hechizos
  
   CREATE OR REPLACE TABLE magias (
    id_magia int AUTO_INCREMENT NOT NULL,
    nombre varchar(100) NOT NULL,
    descripcion text,
    nivel_conjuro enum('Truco','Nivel 1','Nivel 2', 'Nivel 3', 'Nivel 4', 'Nivel 5', 'Nivel 6', 'Nivel 7', 'Nivel 8', 'Nivel 9') NOT NULL,
    escuela enum('Evocación','Necromancia','Adivinación','Transmutación','Encantamiento','Ilusión','Conjuración','Abjuración') NOT NULL,
    rango varchar(100) NOT NULL,
    tiempo_casteo varchar(100) NOT NULL,
    duración varchar(100) NOT NULL,
    concentracion_ritual enum('Concentración','Ritual'),
    PRIMARY KEY (id_magia));

  -- Hechizos que tienen una clase

   CREATE OR REPLACE TABLE magiasclase (
    id int AUTO_INCREMENT NOT NULL,
    id_clase int NOT NULL,
    id_magia int NOT NULL,
    nivel_desbloqueo int NOT NULL,
    PRIMARY KEY (id));

  -- Hechizos que almacenan los personajes

   CREATE OR REPLACE TABLE magiaspersonajes (
    id int AUTO_INCREMENT NOT NULL,
    id_magia int NOT NULL,
    id_personaje int NOT NULL,
    PRIMARY KEY(id));

  -- Materiales de magia

   CREATE OR REPLACE TABLE materialesmagias (
    id int AUTO_INCREMENT NOT NULL,
    id_magia int NOT NULL,
    material enum('Verbal','Somático','Material') NOT NULL,
    PRIMARY KEY(id));

  -- Dotes

   CREATE OR REPLACE TABLE dotes (
    id_dote int AUTO_INCREMENT NOT NULL,
    titulo varchar(100) NOT NULL,
    descripcion text,
    fuerza int,
    destreza int,
    constitucion int,
    inteligencia int,
    sabiduria int,
    carisma int,
    PRIMARY KEY(id_dote));

  -- Los personajes tienen dotes

   CREATE OR REPLACE TABLE dotesdelospersonajes (
    id int AUTO_INCREMENT NOT NULL,
    id_dote int NOT NULL,
    id_personaje int NOT NULL,
    PRIMARY KEY(id));  

  -- Sesiones

   CREATE OR REPLACE TABLE sesiones (
    id_sesion int AUTO_INCREMENT NOT NULL,
    duracion varchar(20),
    num_miembros int NOT NULL,
    nivel_medio int NOT NULL,
    fecha date NOT NULL,
    PRIMARY KEY(id_sesion));

  -- Personajes están en sesiones

   CREATE OR REPLACE TABLE personajesesion (
    id int AUTO_INCREMENT NOT NULL,
    id_personaje int NOT NULL,
    id_sesion int NOT NULL,
    PRIMARY KEY (id));
  
  -- Trasfondos
    
   CREATE OR REPLACE TABLE trasfondos (        
    id_trasfondo int AUTO_INCREMENT NOT NULL,
    nombre varchar(100) NOT NULL,
    descripcion text,
    equipo text,
    herramientas text,
    PRIMARY KEY (id_trasfondo));

  -- idiomas que te dan los trasfondos

   CREATE OR REPLACE TABLE idiomastrasfondos (
    id int AUTO_INCREMENT NOT NULL,
    id_trasfondo int NOT NULL,
    idioma varchar(100),
    PRIMARY KEY (id));

  -- Competencia con habilidades que te dan los trasfondos

   CREATE OR REPLACE TABLE comphabilidades (
    id int AUTO_INCREMENT NOT NULL,
    id_trasfondo int NOT NULL,
    habilidad varchar(20) NOT NULL,
    PRIMARY KEY(id));

  -- Recomendaciones Armaduras 
    
    CREATE OR REPLACE TABLE recomarmaduras (
     id int AUTO_INCREMENT NOT NULL,
     id_usuario int NOT NULL,
     id_armadura int NOT NULL,
     puntuación float NOT NULL,
     reseña text,
     PRIMARY KEY(id));
     
  -- Recomendaciones Armas 
    
    CREATE OR REPLACE TABLE recomarmas (
     id int AUTO_INCREMENT NOT NULL,
     id_usuario int NOT NULL,
     id_arma int NOT NULL,
     puntuación float NOT NULL,
     reseña text,
     PRIMARY KEY(id));
     
  -- Recomendaciones Clases
    
    CREATE OR REPLACE TABLE recomclases (
     id int AUTO_INCREMENT NOT NULL,
     id_usuario int NOT NULL,
     id_clase int NOT NULL,
     puntuación float NOT NULL,
     reseña text,
     PRIMARY KEY(id));
     
  -- Recomendaciones Dotes 
    
    CREATE OR REPLACE TABLE recomdotes (
     id int AUTO_INCREMENT NOT NULL,
     id_usuario int NOT NULL,
     id_dote int NOT NULL,
     puntuación float NOT NULL,
     reseña text,
     PRIMARY KEY(id));
     
  -- Recomendaciones Magias 
    
    CREATE OR REPLACE TABLE recommagias (
     id int AUTO_INCREMENT NOT NULL,
     id_usuario int NOT NULL,
     id_magia int NOT NULL,
     puntuación float NOT NULL,
     reseña text,
     PRIMARY KEY(id));
     
  -- Recomendaciones Objetos 
    
    CREATE OR REPLACE TABLE recomobjetos (
     id int AUTO_INCREMENT NOT NULL,
     id_usuario int NOT NULL,
     id_objeto int NOT NULL,
     puntuación float NOT NULL,
     reseña text,
     PRIMARY KEY(id));
     
  -- Recomendaciones Razas 
    
    CREATE OR REPLACE TABLE recomrazas (
     id int AUTO_INCREMENT NOT NULL,
     id_usuario int NOT NULL,
     id_raza int NOT NULL,
     puntuación float NOT NULL,
     reseña text,
     PRIMARY KEY(id));
     
  -- Recomendaciones Trasfondos 
    
    CREATE OR REPLACE TABLE recomtrasfondos (
     id int AUTO_INCREMENT NOT NULL,
     id_usuario int NOT NULL,
     id_trasfondo int NOT NULL,
     puntuación float NOT NULL,
     reseña text,
     PRIMARY KEY(id));
     

-- Foreign Keys y Unique Keys
 -- Personajes

  ALTER TABLE personajes 
    
    ADD CONSTRAINT fk_personajes_usuarios
    FOREIGN KEY (id_usuario)
    REFERENCES usuarios(id_usuario)
    ON DELETE CASCADE ON UPDATE CASCADE,

    ADD CONSTRAINT fk_personajes_armaduras
    FOREIGN KEY (id_armadura)
    REFERENCES armaduras(id_armaduras)
    ON DELETE CASCADE ON UPDATE CASCADE,

    ADD CONSTRAINT fk_personajes_razas
    FOREIGN KEY (id_raza)
    REFERENCES razas(id_raza)
    ON DELETE CASCADE ON UPDATE CASCADE,

    ADD CONSTRAINT fk_personajes_trasfondo
    FOREIGN KEY (id_trasfondo)
    REFERENCES trasfondos(id_trasfondo)
    ON DELETE CASCADE ON UPDATE CASCADE; 

 -- idiomas del personaje

   ALTER TABLE idiomaspersonaje

    ADD CONSTRAINT fk_idiomaspersonaje_personajes
    FOREIGN KEY (id_personaje)
    REFERENCES personajes(id_personaje)
    ON DELETE CASCADE ON UPDATE CASCADE,

    ADD CONSTRAINT uk_personaje_idioma
    UNIQUE KEY (id_personaje,idioma);

 -- Armas del personaje

  ALTER TABLE armaspersonaje

    ADD CONSTRAINT fk_armaspersonaje_personaje
    FOREIGN KEY (id_personaje)
    REFERENCES personajes(id_personaje)
    ON DELETE CASCADE ON UPDATE CASCADE,

    ADD CONSTRAINT fk_armaspersonaje_armas
    FOREIGN KEY (id_arma)
    REFERENCES armas(id_arma)
    ON DELETE CASCADE ON UPDATE CASCADE,

    ADD CONSTRAINT uk_armas_personajes
    UNIQUE KEY(id_arma,id_personaje);

 -- Objetos del personaje

  ALTER TABLE objetosdelpersonaje

    ADD CONSTRAINT fk_objetosdelpersonaje_personaje
    FOREIGN KEY (id_personaje)
    REFERENCES personajes(id_personaje)
    ON DELETE CASCADE ON UPDATE CASCADE,

    ADD CONSTRAINT fk_objetosdelpersonaje_objetos
    FOREIGN KEY (id_objeto)
    REFERENCES objetos(id_objeto)
    ON DELETE CASCADE ON UPDATE CASCADE,

    ADD CONSTRAINT uk_objetos_personaje
    UNIQUE KEY (id_personaje,id_objeto);

 -- Idiomas del personaje

  ALTER TABLE idiomasdelaraza

    ADD CONSTRAINT fk_idiomasdelaraza_raza
    FOREIGN KEY (id_raza)
    REFERENCES razas(id_raza)
    ON DELETE CASCADE ON UPDATE CASCADE,

    ADD CONSTRAINT uk_razas_idiomas
    UNIQUE KEY (id_raza,idioma);

 -- Tienen razas

  ALTER TABLE tienenrazas

    ADD CONSTRAINT fk_tienenr_razas
    FOREIGN KEY (id_raza)
    REFERENCES razas(id_raza)
    ON DELETE CASCADE ON UPDATE CASCADE,

    ADD CONSTRAINT fk_tienenr_habilidadrazas
    FOREIGN KEY (id_habilidadesraza)
    REFERENCES habilidadesraza(id_habilidadraza)
    ON DELETE CASCADE ON UPDATE CASCADE,

    ADD CONSTRAINT uk_razas_habilidadesrazas
    UNIQUE KEY (id_raza,id_habilidadesraza);

 -- Competencia con habilidades que te da las clases

  ALTER TABLE clasecomphabilidades

    ADD CONSTRAINT fk_clasecomphabilidades_clase
    FOREIGN KEY (id_clase)
    REFERENCES clases(id_clase)
    ON DELETE CASCADE ON UPDATE CASCADE,

    ADD CONSTRAINT uk_clase_habilidad
    UNIQUE KEY (id_clase,habilidad);

 -- Clases de los personajes

  ALTER TABLE clasesdelospersonajes

    ADD CONSTRAINT fk_clasesdelospersonajes_clase
    FOREIGN KEY (id_clase)
    REFERENCES clases(id_clase)
    ON DELETE CASCADE ON UPDATE CASCADE,

    ADD CONSTRAINT fk_clasesdelospersonajes_personaje
    FOREIGN KEY (id_personaje)
    REFERENCES personajes(id_personaje)
    ON DELETE CASCADE ON UPDATE CASCADE,

    ADD CONSTRAINT uk_clase_personaje
    UNIQUE KEY (id_clase,id_personaje);

 -- Salvaciones que te da la clase

  ALTER TABLE salvaciones

    ADD CONSTRAINT fk_salvaciones_clase
    FOREIGN KEY (id_clase)
    REFERENCES clases(id_clase)
    ON DELETE CASCADE ON UPDATE CASCADE,

    ADD CONSTRAINT uk_clases_salvaciones
    UNIQUE KEY (id_clase,salvaciones);


 -- Tienen clases

  ALTER TABLE tienenclase

    ADD CONSTRAINT fk_tienenclase_clases
    FOREIGN KEY (id_clase)
    REFERENCES clases(id_clase)
    ON DELETE CASCADE ON UPDATE CASCADE,

    ADD CONSTRAINT fk_tienenclase_habilidadesclase
    FOREIGN KEY (id_habilidadesclase)
    REFERENCES habilidadesclase(id_habilidadesclase)
    ON DELETE CASCADE ON UPDATE CASCADE,

    ADD CONSTRAINT uk_clase_habilidadesclase
    UNIQUE KEY (id_clase,id_habilidadesclase);

 -- Magias de las clases

  ALTER TABLE magiasclase

    ADD CONSTRAINT fk_magiasclase_clase
    FOREIGN KEY (id_clase)
    REFERENCES clases(id_clase)
    ON DELETE CASCADE ON UPDATE CASCADE,

    ADD CONSTRAINT fk_magiasclase_magias
    FOREIGN KEY (id_magia)
    REFERENCES magias(id_magia)
    ON DELETE CASCADE ON UPDATE CASCADE,

    ADD CONSTRAINT uk_clase_magia
    UNIQUE KEY (id_clase,id_magia);

 -- Materiales de las magias

 ALTER TABLE materialesmagias

    ADD CONSTRAINT fk_materialesmagias_magias
    FOREIGN KEY (id_magia)
    REFERENCES magias(id_magia)
    ON DELETE CASCADE ON UPDATE CASCADE,

    ADD CONSTRAINT uk_materiales_magia
    UNIQUE KEY (material,id_magia);

 -- Magias de los personajes 

  ALTER TABLE magiaspersonajes

    ADD CONSTRAINT fk_magiaspersonjes_personaje
    FOREIGN KEY (id_personaje)
    REFERENCES personajes(id_personaje)
    ON DELETE CASCADE ON UPDATE CASCADE,

    ADD CONSTRAINT fk_magiaspersonajes_magias
    FOREIGN KEY (id_magia)
    REFERENCES magias(id_magia)
    ON DELETE CASCADE ON UPDATE CASCADE,

    ADD CONSTRAINT uk_personaje_magia
    UNIQUE KEY (id_personaje,id_magia);


 -- Los personajes tienen dotes

  ALTER TABLE dotesdelospersonajes

    ADD CONSTRAINT fk_dotesdelospersonajes_dotes
    FOREIGN KEY (id_dote)
    REFERENCES dotes(id_dote)
    ON DELETE CASCADE ON UPDATE CASCADE,

    ADD CONSTRAINT fk_dotesdelospersonajes_personaje
    FOREIGN KEY (id_personaje)
    REFERENCES personajes(id_personaje)
    ON DELETE CASCADE ON UPDATE CASCADE,
    
    ADD CONSTRAINT uk_personajes_dotes
    UNIQUE KEY(id_personaje,id_dote);

 -- Personajes de la sesión

  ALTER TABLE personajesesion

    ADD CONSTRAINT fk_personajesesion_personajes
    FOREIGN KEY (id_personaje)
    REFERENCES personajes(id_personaje)
    ON DELETE CASCADE ON UPDATE CASCADE,

    ADD CONSTRAINT fk_personajesesion_sesion
    FOREIGN KEY (id_sesion)
    REFERENCES sesiones(id_sesion)
    ON DELETE CASCADE ON UPDATE CASCADE,

    ADD CONSTRAINT uk_personaje_sesion
    UNIQUE KEY (id_personaje,id_sesion);
  
 -- Idiomas que te dan los trasfondos

  ALTER TABLE idiomastrasfondos

    ADD CONSTRAINT fk_idiomastrafondo_trasfondo
    FOREIGN KEY (id_trasfondo)
    REFERENCES trasfondos(id_trasfondo)
    ON DELETE CASCADE ON UPDATE CASCADE,

    ADD CONSTRAINT uk_trasfondo_idiomas
    UNIQUE KEY (id_trasfondo,idioma);

 -- Competencia con habilidades que te da los trasfondos

  ALTER TABLE comphabilidades

    ADD CONSTRAINT fk_comphabilidades_trasfondo
    FOREIGN KEY (id_trasfondo)
    REFERENCES trasfondos(id_trasfondo)
    ON DELETE CASCADE ON UPDATE CASCADE,

    ADD CONSTRAINT uk_trasfondo_habilidad
    UNIQUE KEY (id_trasfondo,habilidad);

 -- Recomendacion armaduras

  ALTER TABLE recomarmaduras

    ADD CONSTRAINT fk_recomenarmaduras_usuarios
    FOREIGN KEY (id_usuario)
    REFERENCES usuarios(id_usuario)
    ON DELETE CASCADE ON UPDATE CASCADE,

    ADD CONSTRAINT fk_recomenarmaduras_armaduras
    FOREIGN KEY (id_armadura)
    REFERENCES armaduras(id_armaduras)
    ON DELETE CASCADE ON UPDATE CASCADE,

    ADD CONSTRAINT uk_armaduras_usuarios
    UNIQUE KEY (id_usuario,id_armadura);

 -- Recomendacion armas

  ALTER TABLE recomarmas

    ADD CONSTRAINT fk_recomenarmas_usuarios
    FOREIGN KEY (id_usuario)
    REFERENCES usuarios(id_usuario)
    ON DELETE CASCADE ON UPDATE CASCADE,

    ADD CONSTRAINT fk_recomenarmas_armas
    FOREIGN KEY (id_arma)
    REFERENCES armas(id_arma)
    ON DELETE CASCADE ON UPDATE CASCADE,

    ADD CONSTRAINT uk_armas_usuarios
    UNIQUE KEY (id_usuario,id_arma);

 -- Recomendacion Clases

  ALTER TABLE recomclases

    ADD CONSTRAINT fk_recomclases_usuarios
    FOREIGN KEY (id_usuario)
    REFERENCES usuarios(id_usuario)
    ON DELETE CASCADE ON UPDATE CASCADE,

    ADD CONSTRAINT fk_recomclases_clases
    FOREIGN KEY (id_clase)
    REFERENCES clases(id_clase)
    ON DELETE CASCADE ON UPDATE CASCADE,

    ADD CONSTRAINT uk_clases_usuarios
    UNIQUE KEY (id_usuario,id_clase);

 -- Recomendacion Dotes

  ALTER TABLE recomdotes

    ADD CONSTRAINT fk_recomdotes_usuarios
    FOREIGN KEY (id_usuario)
    REFERENCES usuarios(id_usuario)
    ON DELETE CASCADE ON UPDATE CASCADE,

    ADD CONSTRAINT fk_recomdotes_dotes
    FOREIGN KEY (id_dote)
    REFERENCES dotes(id_dote)
    ON DELETE CASCADE ON UPDATE CASCADE,

    ADD CONSTRAINT uk_dotes_usuarios
    UNIQUE KEY (id_usuario,id_dote);

 -- Recomendacion Magias

  ALTER TABLE recommagias

    ADD CONSTRAINT fk_recommagias_usuarios
    FOREIGN KEY (id_usuario)
    REFERENCES usuarios(id_usuario)
    ON DELETE CASCADE ON UPDATE CASCADE,

    ADD CONSTRAINT fk_recommagias_magias
    FOREIGN KEY (id_magia)
    REFERENCES magias(id_magia)
    ON DELETE CASCADE ON UPDATE CASCADE,

    ADD CONSTRAINT uk_magias_usuarios
    UNIQUE KEY (id_usuario,id_magia);

 -- Recomendacion Objetos

  ALTER TABLE recomobjetos

    ADD CONSTRAINT fk_recomobjetos_usuarios
    FOREIGN KEY (id_usuario)
    REFERENCES usuarios(id_usuario)
    ON DELETE CASCADE ON UPDATE CASCADE,

    ADD CONSTRAINT fk_recomobjetos_objetos
    FOREIGN KEY (id_objeto)
    REFERENCES objetos(id_objeto)
    ON DELETE CASCADE ON UPDATE CASCADE,

    ADD CONSTRAINT uk_objetos_usuarios
    UNIQUE KEY (id_usuario,id_objeto);

 -- Recomendacion Razas

  ALTER TABLE recomrazas

    ADD CONSTRAINT fk_recomrazas_usuarios
    FOREIGN KEY (id_usuario)
    REFERENCES usuarios(id_usuario)
    ON DELETE CASCADE ON UPDATE CASCADE,

    ADD CONSTRAINT fk_recomrazas_razas
    FOREIGN KEY (id_raza)
    REFERENCES razas(id_raza)
    ON DELETE CASCADE ON UPDATE CASCADE,

    ADD CONSTRAINT uk_raza_usuarios
    UNIQUE KEY (id_raza,id_usuario);

 -- Recomendacion Trasfondos

  ALTER TABLE recomtrasfondos

    ADD CONSTRAINT fk_recomtrasfondos_usuarios
    FOREIGN KEY (id_usuario)
    REFERENCES usuarios(id_usuario)
    ON DELETE CASCADE ON UPDATE CASCADE,

    ADD CONSTRAINT fk_recomtrasfondos_armaduras
    FOREIGN KEY (id_trasfondo)
    REFERENCES trasfondos(id_trasfondo)
    ON DELETE CASCADE ON UPDATE CASCADE,

    ADD CONSTRAINT uk_trasfondos_usuarios
    UNIQUE KEY (id_usuario,id_trasfondo);


    
    





    








